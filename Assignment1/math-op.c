#include <stdio.h>

/*
 * Add two numbers and print the result to the screen
 */

int main()
{
	// Declare two variables to keep our number input.
	int number1, number2;

	//Prompt the user for input values
	printf("Enter your first number to add: ");
	scanf("%i", &number1);
	printf("Enter your second number to add: ");
	scanf("%i", &number2);

	// Add the two numbers and save the result in a separate variable
	int result = number1 + number2;

	// Print the result to the screen
	printf("The result of adding my two numbers is: %d", result);

	return 1;
}
